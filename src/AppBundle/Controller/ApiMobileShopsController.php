<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;


class ApiMobileShopsController extends Controller
{
    /**
     * @Route("/api/shops/list_all")
     * @Method("GET")
     */
    public function ListAllAction()
    {
      $data = $this->getDoctrine()
        ->getRepository('AppBundle:Shop')
        ->createQueryBuilder('e')
        ->select('e')
        ->getQuery()
        ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

      $response = new Response(
        json_encode($data),
        Response::HTTP_OK,
        array(
          'Access-Control-Allow-Origin' => '*',
          'content-type' => 'application/json'
        ));
      return $response;
    }

    /**
     * @Route("/api/shops/add/{name}/{category}/{position}")
     * @Method("POST")
     */
    public function AddShopAction($name, $category, $position)
    {
      $product = new Shop();
      $product->setName($name);
      $product->setCategory($category);
      $product->setPosition($position);

      $em = $this->getDoctrine()->getManager();

      // tells Doctrine you want to (eventually) save the Product (no queries yet)
      $em->persist($product);

      // actually executes the queries (i.e. the INSERT query)
      $em->flush();

      return new Response('Saved new product with id '.$product->getId());
    }

}
